/*
    CASIO PRIZM version of the well-known Othello board game.
    Copyright (C) 2012-2018  Balázs Dura-Kovács

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Functions below by Christopher Mitchell (c) 2011
	https://www.cemetech.net/forum/viewtopic.php?t=6114
	http://prizm.cemetech.net/index.php/Useful_Routines
*/

#include <fxcg/display.h>

void * VRAMAdress;

void fillArea(unsigned x,unsigned y,unsigned w,unsigned h,unsigned short col){
	unsigned short*s=(unsigned short*)VRAMAdress;
	s+=(y*384)+x;
	while(h--){
		unsigned w2=w;
		while(w2--)
			*s++=col;
		s+=384-w;
	}
}


void plot(unsigned x,unsigned y,unsigned short color){
	unsigned short*s=(unsigned short*)VRAMAdress;
	s+=(y*384)+x;
	*s=color;
}

void CopySpriteNbit(const unsigned char* data, int x, int y, int width, int height, color_t* palette, unsigned int bitwidth) {
	color_t* VRAM = (color_t*) VRAMAdress;
	VRAM += (LCD_WIDTH_PX*y + x);
	int offset = 0;
	unsigned char buf;
	for(int j=y; j<y+height; j++) {
		int availbits = 0;
		for(int i=x; i<x+width;  i++) {
			if (!availbits) {
				buf = data[offset++];
				availbits = 8;
			}
			color_t this = ((color_t)buf>>(8-bitwidth));
			*VRAM = palette[(color_t)this];
			VRAM++;
			buf<<=bitwidth;
			availbits-=bitwidth;
		}
		VRAM += (LCD_WIDTH_PX-width);
	}
}
