/*
    CASIO PRIZM version of the well-known Othello board game.
    Copyright (C) 2012-2018  Balázs Dura-Kovács

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

unsigned char tabla[16] = {0, 0, 0, 0, 0, 0, 2, 64, 1, 128, 0, 0, 0, 0, 0, 0}; //egy mező 2bit; 0:üres-nem lehet; 1:fekete; 2:fehér; 3:üres-lehet

unsigned char kurzorx=3;
unsigned char kurzory=3;

unsigned char jatekos=1;
unsigned char jatek=1;

char jatekos_nev[3][11] = {
    "xxComputer",
    "xxPlayer 1",
    "xxPlayer 2"
};

const char key[18] = {187, 102, 204, 96, 59, 47, 253, 127, 160, 42, 112, 88, 170, 158, 223, 140, 129, 139};

const unsigned short HSFile[] = {'\\','\\','f','l','s','0','\\','O','T','H','E','L','L','O','_','0','1','\0'};

unsigned char volte_elozo = 2; //első vizsgálat előtt: 2; első vizsgálat után nem volt: 0; első vizsgálat után volt: 2; második vizsgálat után: 2

const color_t kurzor_szinek[4] = {COLOR_RED, COLOR_RED, COLOR_RED, COLOR_GOLD};

unsigned char sum_fekete=2, sum_feher=2;
