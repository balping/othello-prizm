/*
    CASIO PRIZM version of the well-known Othello board game.
    Copyright (C) 2012-2018  Balázs Dura-Kovács

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "utilities.h"
#include <keyboard.h>
#include <keyboard.hpp>

#include "data.h"
#include "../img/korong.h"
#include "../img/korong_print.h"
#include "edit/LineEditors.c"

#define get_mezo(x,y) ((tabla[(y)*2+(x)/4] >> (6-2*((x)%4))) & (unsigned char)3)
void set_mezo(unsigned char x, unsigned char y,  unsigned char ertek);
void PrintXYArray(int x, int y, int j, int mode, int color);
void wait_exit();
void vege();
void lehetoseg_szamol();
void lep();
void status();
unsigned char jatekos_nev_hossz(unsigned char p);
void new_game();
void setup();
void save();
void load();

#define tabla_x 1
#define tabla_y 22

#define px(x) (tabla_x+1+24*(x))
#define py(y) (tabla_y+1+24*(y))

#define PRINTX(xx) (18*(xx)-18)
#define PRINTY(yy) (24*(yy))

typedef enum {rm_tabla, rm_korong, rm_kurzor, rm_jatekos, rm_status, rm_minden} rajzmodok;
void rajzol(rajzmodok mod);

int main(void){
	Bdisp_EnableColor(1);
	EnableStatusArea(0);
	DefineStatusAreaFlags(DSA_SETDEFAULT, 0, 0, 0);
	DefineStatusAreaFlags(3, SAF_BATTERY | SAF_TEXT | SAF_GLYPH | SAF_ALPHA_SHIFT, 0, 0);
	DefineStatusMessage("Othello",1,0,0);
	VRAMAdress = GetVRAMAddress();
	
	load();
	
	lehetoseg_szamol();
	rajzol(rm_minden);
	
	PrintXY(12, 5, "xx[F1]:", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	PrintXY(12, 6, "xx[F2]:", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	PrintXY(12, 7, "xx[F3]:", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	const color_t fkey_palette[2] = {COLOR_WHITE, COLOR_BLACK};
	int result;
	GetFKeyPtr(0x0186, &result);//NEW
	CopySpriteNbit(result, PRINTX(19)-5, PRINTY(5), 64, 22, fkey_palette, 1);
	GetFKeyPtr( 0x03E4, &result );//SETUP
	CopySpriteNbit(result, PRINTX(19)-5, PRINTY(6), 64, 22, fkey_palette, 1);
	GetFKeyPtr( 0x0302, &result );//save
	CopySpriteNbit(result, PRINTX(19)-5, PRINTY(7), 64, 22, fkey_palette, 1);
	
	int key;
	while(1){
		GetKey(&key);
		if(jatek){//menet közbeni kezelés
			switch (key){
				case KEY_CTRL_UP:
					kurzory = (kurzory+7)%8;
					rajzol(rm_minden);
					break;
				case KEY_CTRL_DOWN:
					kurzory = (kurzory+1)%8;
					rajzol(rm_minden);
					break;
				case KEY_CTRL_LEFT:
					kurzorx = (kurzorx+7)%8;
					rajzol(rm_minden);
					break;
				case KEY_CTRL_RIGHT:
					kurzorx = (kurzorx+1)%8;
					rajzol(rm_minden);
					break;
				case KEY_CTRL_EXE:
					lep();
					break;
			}
		}
		//általános kezelés
		switch (key){
			case KEY_CTRL_F1:
				new_game();
				break;
			case KEY_CTRL_F2:
				setup();
				break;
			case KEY_CTRL_F3:
				save();
				break;
		}
		
		
	}
	return 0;
}


void rajzol(rajzmodok mod){
	int i;//rm_tabla
	unsigned char x,y,get;//rm_korong
	color_t kurzor_szin;//rm_kurzor
	char buffer[6] = "xx:##";//rm_status
	switch(mod){
		case rm_tabla:
			fillArea(tabla_x+1, tabla_y+1, 191, 191, COLOR_DARKGREEN);
			//vízszintes vonalak
			for(i=0; i<=8; i++){
				fillArea(tabla_x, tabla_y+i*24, 193, 1, COLOR_BLACK);
			}
			//függőleges vonalak
			for(i=0; i<=8; i++){
				fillArea(tabla_x+i*24, tabla_y+1, 1, 192, COLOR_BLACK);
			}
			//kurzor szegélyek törlése
			fillArea(tabla_x, tabla_y-1, 193, 1, COLOR_WHITE);
			fillArea(tabla_x-1, tabla_y-1, 1, 194, COLOR_WHITE);
			fillArea(px(8), tabla_y-1, 1, 194, COLOR_WHITE);
			plot(px(8), tabla_y, COLOR_BLACK);
			fillArea(tabla_x-1, py(8), 194, 1, COLOR_WHITE);
			break;
		case rm_korong:
			for(x=0;x<8;x++){
			for(y=0;y<8;y++){
				get = get_mezo(x, y);
				if(get== 1 || get==2){
					VRAM_CopySprite(korong_kep[get-1], px(x), py(y), KORONG_WIDTH, KORONG_HEIGHT);
				}
			}
			}
			break;
		case rm_kurzor:
			kurzor_szin = kurzor_szinek[get_mezo(kurzorx, kurzory)];
			fillArea(px(kurzorx)-2, py(kurzory)-2, 25, 2, kurzor_szin);
			fillArea(px(kurzorx+1)-1, py(kurzory)-2, 2, 25, kurzor_szin);
			fillArea(px(kurzorx), py(kurzory+1)-1, 25, 2, kurzor_szin);
			fillArea(px(kurzorx)-2, py(kurzory), 2, 25, kurzor_szin);
			break;
		case rm_jatekos:
			PrintXY(12, 1, "xx         ", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
			PrintXYArray(12, 1, jatekos, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
			VRAM_CopySprite(korong_print_kep[jatekos-1], 359, PRINTY(1)-1, KORONG_PRINT_WIDTH, KORONG_PRINT_HEIGHT);
			break;
		case rm_status:
			status();
			PrintXY(13, 3, "xx         ", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
			itoa(sum_fekete, buffer+3);
			PrintXY(13, 3, buffer, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
			itoa(sum_feher, buffer+3);
			PrintXY(19, 3, buffer, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
			VRAM_CopySprite(korong_print_kep[0], PRINTX(12), PRINTY(3), KORONG_PRINT_WIDTH, KORONG_PRINT_HEIGHT);
			VRAM_CopySprite(korong_print_kep[1], PRINTX(18), PRINTY(3), KORONG_PRINT_WIDTH, KORONG_PRINT_HEIGHT);
			break;
		case  rm_minden:
			rajzol(rm_tabla);
			rajzol(rm_korong);
			rajzol(rm_kurzor);
			rajzol(rm_jatekos);
			rajzol(rm_status);
			break;
	}
}

void PrintXYArray(int x, int y, int j, int mode, int color){
	char buff[sizeof(jatekos_nev[j])];
	int i;
	
	for(i=0; i<sizeof(jatekos_nev[j]); i++){
		buff[i] = jatekos_nev[j][i];
	}
	
	PrintXY(x, y, buff, mode, color);
}

void set_mezo(unsigned char x, unsigned char y,  unsigned char ertek){
	unsigned char i,j;
	i=y*2+x/4;
	j=(6-2*((x)%4));
	ertek <<= j;
	tabla[i] &= ~(3 << j );
	tabla[i] |= ertek;
}

void lehetoseg_szamol(){
	unsigned char x, y, lehete;
	signed char xx, yy;
	unsigned char volte = 0;
	for(x=0; x<=7; x++){
	for(y=0; y<=7; y++){
		if(get_mezo(x,y)%3==0){
			//fel
			xx=x; yy=y;
			do{
				yy--;
				lehete=0;
				if(yy>=0){if(get_mezo(xx,yy)==3-jatekos){
					lehete = 1;
				}}
			}while(lehete);
			if(yy>=0 && abs(yy-y)!=1){if(get_mezo(xx,yy)==jatekos){
				lehete = 1;
			}}

			//jobbra-fel
			if(!lehete){
				xx=x; yy=y;
				do{
					yy--; xx++;
					lehete=0;
					if(yy>=0 && xx<=7){if(get_mezo(xx,yy)==3-jatekos){
						lehete = 1;
					}}
				}while(lehete);
				if(yy>=0 && xx<=7 && abs(yy-y)!=1){if(get_mezo(xx,yy)==jatekos){
						lehete = 1;
				}}
			}

			//jobbra
			if(!lehete){
				xx=x; yy=y;
				do{
					xx++;
					lehete=0;
					if(xx<=7){if(get_mezo(xx,yy)==3-jatekos){
						lehete = 1;
					}}
				}while(lehete);
				if(xx<=7 && abs(xx-x)!=1){if(get_mezo(xx,yy)==jatekos){
						lehete = 1;
				}}
			}

			//jobbra-le
			if(!lehete){
				xx=x; yy=y;
				do{
					yy++; xx++;
					lehete=0;
					if(yy<=7 && xx<=7){if(get_mezo(xx,yy)==3-jatekos){
						lehete = 1;
					}}
				}while(lehete);
				if(yy<=7 && xx<=7 && abs(yy-y)!=1){if(get_mezo(xx,yy)==jatekos){
						lehete = 1;
				}}
			}

			//le
			if(!lehete){
				xx=x; yy=y;
				do{
					yy++;
					lehete=0;
					if(yy<=7){if(get_mezo(xx,yy)==3-jatekos){
						lehete = 1;
					}}
				}while(lehete);
				if(yy<=7 && abs(yy-y)!=1){if(get_mezo(xx,yy)==jatekos){
						lehete = 1;
				}}
			}

			//balra-le
			if(!lehete){
				xx=x; yy=y;
				do{
					yy++; xx--;
					lehete=0;
					if(yy<=7 && xx>=0){if(get_mezo(xx,yy)==3-jatekos){
						lehete = 1;
					}}
				}while(lehete);
				if(yy<=7 && xx>=0 && abs(yy-y)!=1){if(get_mezo(xx,yy)==jatekos){
						lehete = 1;
				}}
			}

			//balra
			if(!lehete){
				xx=x; yy=y;
				do{
					xx--;
					lehete=0;
					if(xx>=0){if(get_mezo(xx,yy)==3-jatekos){
						lehete = 1;
					}}
				}while(lehete);
				if(xx>=0 && abs(xx-x)!=1){if(get_mezo(xx,yy)==jatekos){
						lehete = 1;
				}}
			}

			//balra-fel
			if(!lehete){
				xx=x; yy=y;
				do{
					yy--; xx--;
					lehete=0;
					if(yy>=0 && xx>=0){if(get_mezo(xx,yy)==3-jatekos){
						lehete = 1;
					}}
				}while(lehete);
				if(yy>=0 && xx>=0 && abs(yy-y)!=1){if(get_mezo(xx,yy)==jatekos){
						lehete = 1;
				}}
			}
			
			if(lehete){
				set_mezo(x,y,3);
				volte=1;
			}else{
				set_mezo(x,y,0);
				volte = volte || 0;
			}
			
		}
	}
	}
	
	if(volte_elozo==2){//első vizsgálat
		if(volte==0){
			volte_elozo=0;
			jatekos = 3-jatekos;
			lehetoseg_szamol();	
		}else if(volte==1){
			volte_elozo = 2;
			rajzol(rm_minden);
		}
	}else if(volte_elozo==0){//második vizsgálat
		if(volte==1){
			volte_elozo = 2;
			rajzol(rm_minden);
			MsgBoxPush(5);
			/*PrintXY(3, 2, "xxNem tudsz l\xE6\x0Apni!", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
			PrintXY(3, 3, "xxEllenfeled k\xE6\x17vet-", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
			PrintXY(3, 4, "xxkezik.", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);*/
			PrintXY(3, 2, "xxNo move is", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
			PrintXY(3, 3, "xxpossible, your", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
			PrintXY(3, 4, "xxturn ends.", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
			PrintXY_2( TEXT_MODE_NORMAL, 1, 6, 2, TEXT_COLOR_BLACK);
			wait_exit();
			MsgBoxPop();
		}else if(volte==0){
			rajzol(rm_minden);
			vege();
		}
	}
}

void lep(){
	if(get_mezo(kurzorx,kurzory)==3){
	set_mezo(kurzorx, kurzory, jatekos);
	signed char xx, yy;
	unsigned char lehete, x, y;
	x=kurzorx; y=kurzory;
	
	//fel
	xx=x; yy=y;
	do{
		yy--;
		lehete=0;
		if(yy>=0){if(get_mezo(xx,yy)==3-jatekos){
			lehete = 1;
		}}
	}while(lehete);
	if(yy>=0 && abs(yy-y)!=1){if(get_mezo(xx,yy)==jatekos){
		xx=x; yy=y;
		do{
			yy--;
			lehete=0;
			if(yy>=0){if(get_mezo(xx,yy)==3-jatekos){
				set_mezo(xx,yy,jatekos);
				lehete = 1;
			}}
		}while(lehete);
	}}

	//jobbra-fel
	xx=x; yy=y;
	do{
		yy--; xx++;
		lehete=0;
		if(yy>=0 && xx<=7){if(get_mezo(xx,yy)==3-jatekos){
			lehete = 1;
		}}
	}while(lehete);
	if(yy>=0 && xx<=7 && abs(yy-y)!=1){if(get_mezo(xx,yy)==jatekos){
		xx=x; yy=y;
		do{
			yy--; xx++;
			lehete=0;
			if(yy>=0 && xx<=7){if(get_mezo(xx,yy)==3-jatekos){
				set_mezo(xx,yy,jatekos);
				lehete = 1;
			}}
		}while(lehete);
	}}

	//jobbra
	xx=x; yy=y;
	do{
		xx++;
		lehete=0;
		if(xx<=7){if(get_mezo(xx,yy)==3-jatekos){
			lehete = 1;
		}}
	}while(lehete);
	if(xx<=7 && abs(xx-x)!=1){if(get_mezo(xx,yy)==jatekos){
		xx=x; yy=y;
		do{
			xx++;
			lehete=0;
			if(xx<=7){if(get_mezo(xx,yy)==3-jatekos){
				set_mezo(xx,yy,jatekos);
				lehete = 1;
			}}
		}while(lehete);
	}}


	//jobbra-le
	xx=x; yy=y;
	do{
		yy++; xx++;
		lehete=0;
		if(yy<=7 && xx<=7){if(get_mezo(xx,yy)==3-jatekos){
			lehete = 1;
		}}
	}while(lehete);
	if(yy<=7 && xx<=7 && abs(yy-y)!=1){if(get_mezo(xx,yy)==jatekos){
		xx=x; yy=y;
		do{
			yy++; xx++;
			lehete=0;
			if(yy<=7 && xx<=7){if(get_mezo(xx,yy)==3-jatekos){
				set_mezo(xx,yy,jatekos);
				lehete = 1;
			}}
		}while(lehete);
	}}

	//le
	xx=x; yy=y;
	do{
		yy++;
		lehete=0;
		if(yy<=7){if(get_mezo(xx,yy)==3-jatekos){
			lehete = 1;
		}}
	}while(lehete);
	if(yy<=7 && abs(yy-y)!=1){if(get_mezo(xx,yy)==jatekos){
		xx=x; yy=y;
		do{
			yy++;
			lehete=0;
			if(yy<=7){if(get_mezo(xx,yy)==3-jatekos){
				set_mezo(xx,yy,jatekos);
				lehete = 1;
			}}
		}while(lehete);
	}}

	//balra-le
	xx=x; yy=y;
	do{
		yy++; xx--;
		lehete=0;
		if(yy<=7 && xx>=0){if(get_mezo(xx,yy)==3-jatekos){
			lehete = 1;
		}}
	}while(lehete);
	if(yy<=7 && xx>=0 && abs(yy-y)!=1){if(get_mezo(xx,yy)==jatekos){
		xx=x; yy=y;
		do{
			yy++; xx--;
			lehete=0;
			if(yy<=7 && xx>=0){if(get_mezo(xx,yy)==3-jatekos){
				set_mezo(xx,yy,jatekos);
				lehete = 1;
			}}
		}while(lehete);
	}}

	//balra
	xx=x; yy=y;
	do{
		xx--;
		lehete=0;
		if(xx>=0){if(get_mezo(xx,yy)==3-jatekos){
			lehete = 1;
		}}
	}while(lehete);
	if(xx>=0 && abs(xx-x)!=1){if(get_mezo(xx,yy)==jatekos){
		xx=x; yy=y;
		do{
			xx--;
			lehete=0;
			if(xx>=0){if(get_mezo(xx,yy)==3-jatekos){
				set_mezo(xx,yy,jatekos);
				lehete = 1;
			}}
		}while(lehete);
	}}

	//balra-fel
	xx=x; yy=y;
	do{
		yy--; xx--;
		lehete=0;
		if(yy>=0 && xx>=0){if(get_mezo(xx,yy)==3-jatekos){
			lehete = 1;
		}}
	}while(lehete);
	if(yy>=0 && xx>=0 && abs(yy-y)!=1){if(get_mezo(xx,yy)==jatekos){
		xx=x; yy=y;
		do{
			yy--; xx--;
			lehete=0;
			if(yy>=0 && xx>=0){if(get_mezo(xx,yy)==3-jatekos){
				set_mezo(xx,yy,jatekos);
				lehete = 1;
			}}
		}while(lehete);
	}}

	jatekos=3-jatekos;
	lehetoseg_szamol();
	}
}

void wait_exit(){
	int key;
	GetKey(&key);
	while (key!=KEY_CTRL_EXIT){GetKey(&key);}
}

void vege(){
	status();
	jatek=0;

	MsgBoxPush(4);
	if(sum_fekete == sum_feher){
		PrintXYArray(5, 2, "xxIt is a draw!", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	}else{
		unsigned char win = (sum_fekete>sum_feher)?1:2;
		unsigned char win_l = jatekos_nev_hossz(win);
		PrintXYArray(3+(11-win_l)/2, 2, win, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
		PrintXY(4+(11-win_l)/2+win_l, 2, "xxwins!", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	}
	
	
	PrintXY(3, 4, "  Press [F1] to", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	PrintXY(3, 5, "  start a new game", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	
	int key=0;
	while(key!=KEY_CTRL_EXIT && key!=KEY_CTRL_F1){
		GetKey(&key);
		switch(key){
			case KEY_CTRL_F1:
				new_game();
				MsgBoxPop();
				break;
			case KEY_CTRL_EXIT:
				MsgBoxPop();
				break;
		}
	}
}

void status(){
	unsigned char x, y;
	sum_fekete=0, sum_feher=0;
	unsigned char get;
	for(x=0; x<8; x++){
	for(y=0; y<8; y++){
		get = get_mezo(x,y);
		if(get==1){
			sum_fekete++;
		}
		if(get==2){
			sum_feher++;
		}
	}
	}
	
}

unsigned char jatekos_nev_hossz(unsigned char p){
	unsigned char i=10;
	
	while(jatekos_nev[p][i] == 0 || jatekos_nev[p][i] == ' '){i--;}
	
	return i-1;
}

void new_game(){
	if(jatek){
		MsgBoxPush(4);
		PrintXY(3, 2, "xxAre you sure?", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
		PrintXY_2( TEXT_MODE_NORMAL, 1, 4, 3, TEXT_COLOR_BLACK);//YES
		PrintXY_2( TEXT_MODE_NORMAL, 1, 5, 4, TEXT_COLOR_BLACK);//NO
		int key;
		GetKey(&key);
		while (key!=KEY_CTRL_F1 && key!=KEY_CTRL_F6){GetKey(&key);}
		if(key==KEY_CTRL_F6){
			MsgBoxPop();
			return;
		}
	}
	MsgBoxPop();
	int i=0;
	for(i=0; i<16; i++){
		tabla[i]=0;
	}
	set_mezo(3,3,2); set_mezo(4,3,1);
	set_mezo(3,4,1); set_mezo(4,4,2);
	sum_fekete=2; sum_feher=2;
	volte_elozo = 2;
	jatek=1;
	jatekos=1;
	lehetoseg_szamol();
	rajzol(rm_minden);
}

void setup(){
	int curpos = 7;
	char edit = 1;//melyik szöveget szerkesztjük
	unsigned char buff[8];//buffer, ebben történik a szerkeztés
	int i=0;

	
	MsgBoxPush(4);
	int key=0;
	while(key!=KEY_CTRL_EXIT){
		switch(key){
			case KEY_CTRL_EXE:
			case KEY_CTRL_RIGHT:
				if(edit>=1){
					for(i=2; i<=9; i++){
						buff[i-2] = jatekos_nev[edit][i];
					}
					key = InputAny(buff, 6, 2+edit, 8, curpos, emANY);
					if(key == KEY_CTRL_EXE || key == KEY_CTRL_UP || key == KEY_CTRL_DOWN){
						for(i=2; i<=9; i++){
							jatekos_nev[edit][i] = buff[i-2];
						}
					}
					PrintXY(5, 2+edit, "xx          ", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
				}
				break;
			case KEY_CTRL_UP:
				edit = 3-edit;
				break;
			case KEY_CTRL_DOWN:
				edit = 3-edit;
				break;
		}
		PrintXY(9, 2, "xxSETUP", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
		PrintXYArray(6, 3, 1, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
		PrintXYArray(6, 4, 2, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
		if(edit>=1){PrintXYArray(6, 2+edit, edit, TEXT_MODE_INVERT, TEXT_COLOR_BLACK);}
		VRAM_CopySprite(korong_print_kep[0] , PRINTX(16), PRINTY(3)-1, KORONG_PRINT_WIDTH, KORONG_PRINT_HEIGHT);
		VRAM_CopySprite(korong_print_kep[1] , PRINTX(16), PRINTY(4)-1, KORONG_PRINT_WIDTH, KORONG_PRINT_HEIGHT);

		
		GetKey(&key);
	}
	
	
	MsgBoxPop();
	rajzol(rm_jatekos);
}

void save(){
	/*szerkezet:
	0:1  - 16:8	tabla[0-15]; 2 bitenként
	17:1 - 17:3	kurzorx; 3bit
	17:4 - 17:6	kurzory; 3bit
	17:7		jatekos-1; 1bit
	17:8		jatek; 1bit
	18   - 33	jatekos_nev[1-2]; 8byteonként*/
	
	unsigned char out[34];
	unsigned char buff=0;
	//tabla
	int i=0;
	for(i=0; i<16; i++){
		out[i] = tabla[i];
	}
	
	//kurzorx
	buff=kurzorx;
	
	//kurzory
	buff <<= 3;
	buff += kurzory;
	
	//jatekos
	buff <<=1;
	buff += jatekos-1;
	
	//jatek
	buff <<=1;
	buff += jatek;
	
	out[17] = buff;
	
	//jatekos_nevek
	for(i=2; i<10; i++){
		out[i+16] = jatekos_nev[1][i];
		out[i+24] = jatekos_nev[2][i];
	}
	
	//elkódolás
	for(i=0; i<18; i++){
		out[i] ^= key[i];
	}

	//Check for existence, create if doesn't exist
	int fh = -1;
	if(0 > (fh = Bfile_OpenFile_OS(HSFile,0x01))){		 //_OPENMODE_READ
		int size = 35;
		if(0 > Bfile_CreateEntry_OS(HSFile,1,&size)){
			DisplayMessageBox("HS File Error!");
			return;
		}else{
			if(0 <= (fh = Bfile_OpenFile_OS(HSFile,0x02))){			//_OPENMODE_WRITE
				Bfile_WriteFile_OS(fh,"Hali",4);
			}
		}
	}
	if(fh >= 0){
		Bfile_CloseFile_OS(fh);
	}
	
	// Re-saving the othello table.
	if(0 <= (fh = Bfile_OpenFile_OS(HSFile,0x02))){			//_OPENMODE_WRITE
		Bfile_WriteFile_OS(fh,out,35);
		Bfile_CloseFile_OS(fh);
	} 
}

void load(){

	unsigned char buff=0;
	unsigned char in[34];
	int i;
	int fh = -1;
	if(0 <= (fh = Bfile_OpenFile_OS(HSFile,0x01))){			//_OPENMODE_READ
		Bfile_ReadFile_OS(fh,in,34,0);
		Bfile_CloseFile_OS(fh);
	
		//decode
		for(i=0; i<18; i++){
			in[i] ^= key[i];
		}
	
		//tabla
		for(i=0; i<16; i++){
			tabla[i] = in[i];
		}
		
		//kurzorx
		kurzorx = in[17] >> 5;
		
		//kurzory
		kurzory = (in[17] & 28) >> 2;
		
		//jatekos
		jatekos = ((in[17] & 2) >> 1)+1;
		
		//jatek
		jatek = in[17] & 1;
	
	
		for(i=2; i<10; i++){
			jatekos_nev[1][i] = in[i+16];
			jatekos_nev[2][i] = in[i+24];
		}
		
		status();
		DisplayMessageBox("Game loaded");
		
	
	}
	
}

